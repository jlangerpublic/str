# SimpleTemplateRender

## How to install
Coming soon.

## How it works
This templating engine works in two parts:
1. It takes the template-string (the HTML with the {}) and splits it up into tokens. (see Tokenizer)
2. It takes the list of tokens and renders valid HTML5 (plain HTML) (see TemplateRenderer)

### Tokeninizer
The Tokenizer divides the document into tokens. Tokens can be thought of as atoms. Which means that every single token has only one meaning. For example this peace of code
```html
<span>{$hello}</span>
```
This consists of three parts. The `<span>` which for the tokenizer is just text because it doesn't care for HTML itself. A variable token `{$hello}` and the trailing `</span>` which is just text again. In the step of processing the document that way the tokenizer creates a typed token. In this case it will create the following Token list.
```php
[
    LiteralToken ('<span>')
    ExpressionToken ('$hello')
    LiteralToken ('</span>')
]
```
Notice that in the token parameters it removed the {} already so the only information that is relevant later is kept. Now we know what the parts of the documents are doing and how they should be handled. (Every token type has its special treatment.)

### TemplateRenderer
The Template renderer takes this list of tokens which where generated in the previous step and turns this list back into HTML which the browser can read. For the literal tokens this is quite easy as the renderer just takes the token's content and returns it. An if token however has a more complex structure of dealing with it. Let's look at an example:
```html
I like {if $food === "apples"}apples{else}bananas{/end}.
```

This will produce a token list like this:
```php
[
    LiteralToken ('I like ')
    IfToken ('$food == "apples"')
    LiteralToken ('apples')
    ElseToken ()
    LiteralToken ('bananas')
    EndToken ()
    LiteralToken ('.')
]
```

In this case a straint forward rendering would not be possible as we have to render certain parts of the document if the condition is true / false. In this case we use a concept called stack. And in this case it works like this:
> The LiteralToken can be rendered no matter what. After we have rendered the token. We can remove this token of the stack as we have processed it. Then we encounter the if token. In there we evalulate the condition and check if it's true. If it is true we need to find the part the templating engine has to render. Which in this case is everything just the following literal token. So we remove the IfToken from the stack because we processed and evaluated that. We also remove the LiteralToken which should be rendered of the stack and pass it to the TemplateRenderer again so that it gets processed. Now we are left with this:
>
```php
    ElseToken ()
    LiteralToken ('bananas')
    EndToken ()
    LiteralToken ('.')
```
>But now the if statement has been completely handled so we need to remove the rest of the if statement (in this case the else block and the end token.) So we remove tokens as long as we don't see the end token (special handling for multi-level if statements included). After all the leftovers were removed now the only token that is left is the LiteralToken('.'). Which is also rendered.

### ExpressionEvaluator
This class is taking a string like `3 > 10` or `strlen($hello)` and converts them into a value. For this it needs the initial values for the variables like in this example the variable `hello` to resolve the second expression. This process just substitutes the variable in and passes it of to the php processor to use the eval function to evaluate the expression.
