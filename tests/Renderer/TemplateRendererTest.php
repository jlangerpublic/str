<?php
declare(strict_types=1);

namespace Tests\JLanger\TemplateEngine\Renderer;

use JLanger\TemplateEngine\Parser\Tokens\EndToken;
use JLanger\TemplateEngine\Parser\Tokens\IfToken;
use JLanger\TemplateEngine\Parser\Tokens\LiteralToken;
use JLanger\TemplateEngine\Renderer\ExpressionEvaluator;
use JLanger\TemplateEngine\Renderer\TemplateRenderer;
use PHPUnit\Framework\TestCase;

class TemplateRendererTest extends TestCase
{
    public function testIf()
    {
        $tr             = new TemplateRenderer(new ExpressionEvaluator());
        $renderedResult = $tr->render([
            new IfToken('{if 1===1'),
            new LiteralToken('hello'),
            new EndToken('')
        ], []);
        $this->assertSame($renderedResult, 'hello');
    }
}
