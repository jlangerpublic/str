<?php
declare(strict_types=1);

namespace Test\Jlanger\TemplateEngine\Renderer;

use JLanger\TemplateEngine\Parser\Interfaces\ExpressionEvaluatorInterface;
use JLanger\TemplateEngine\Renderer\ExpressionEvaluator;
use PHPUnit\Framework\TestCase;

class ExpressionEvaluatorTest extends TestCase
{
    /** @var ExpressionEvaluatorInterface */
    private $ee;

    protected function setUp()
    {
        $this->ee = new ExpressionEvaluator();
    }

    public function testEvaluator()
    {
        $ee = $this->ee->evaluate('1===1', []);
        $this->assertTrue($ee);
    }
}
