<?php
declare(strict_types=1);

namespace Tests\JLanger\TemplateEngine;

use JLanger\TemplateEngine\Parser\Interfaces\TemplateParserInterface;
use JLanger\TemplateEngine\Parser\Interfaces\TemplateRendererInterface;
use JLanger\TemplateEngine\Parser\Tokens\LiteralToken;
use JLanger\TemplateEngine\TemplateEngine;
use JLanger\TemplateEngine\TemplateEngineInterface;
use PHPUnit\Framework\TestCase;

class TemplateEngineTest extends TestCase
{
    /**
     * @var TemplateRendererInterface
     */
    private $tr;

    /**
     * @var TemplateParserInterface
     */
    private $tp;

    /**
     * @var TemplateEngineInterface
     */
    private $str;

    protected function setUp(): void
    {
        $this->tp  = $this->createMock(TemplateParserInterface::class);
        $this->tr  = $this->createMock(TemplateRendererInterface::class);
        $this->str = new TemplateEngine($this->tp, $this->tr);
    }

    public function testRenderRaw(): void
    {
        $tokens = [
            new LiteralToken('Hello')
        ];
        $this->tp->method('parse')->with('Some string')->willReturn($tokens);

        $this->tr->method('render')->with($tokens)->willReturn('Hello');

        $this->assertSame($this->str->renderRaw('Some string'), 'Hello');
    }


    public function testRenderRawWithVariables(): void
    {
        $tokens = [
            new LiteralToken('Hello')
        ];
        $this->tp->method('parse')->with('Some string')->willReturn($tokens);

        $this->tr->method('render')->with($tokens, ['pups' => 'hello'])->willReturn('Hello');

        $this->assertSame($this->str->renderRaw('Some string', ['pups' => 'hello']), 'Hello');
    }


    public function testRenderWithVariables(): void
    {
        $tokens = [
            new LiteralToken('Hello')
        ];

        $this->tp->method('parse')->with('{if 1==1}
Hello
{/end}')->willReturn($tokens);

        $this->tr->method('render', ['hello' => 1])->with($tokens)->willReturn('Hello');

        $this->assertSame($this->str->render(__DIR__.'/test.html', ['hello' => 1]), 'Hello');
    }

    public function testRender(): void
    {
        $tokens = [
            new LiteralToken('Hello')
        ];
        $this->tp->method('parse')->with('{if 1==1}
Hello
{/end}')->willReturn($tokens);

        $this->tr->method('render')->with($tokens)->willReturn('Hello');

        $this->assertSame($this->str->render(__DIR__.'/test.html'), 'Hello');
    }
}
