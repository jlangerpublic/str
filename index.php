<?php
declare(strict_types=1);

use JLanger\TemplateEngine\Parser\CommandTokenParser;
use JLanger\TemplateEngine\Parser\TemplateParser;
use JLanger\TemplateEngine\Renderer\ExpressionEvaluator;
use JLanger\TemplateEngine\Renderer\TemplateRenderer;
use JLanger\TemplateEngine\TemplateEngine;

require __DIR__ . '/vendor/autoload.php';
$STR = new TemplateEngine(new TemplateParser(new CommandTokenParser()), new
TemplateRenderer(new ExpressionEvaluator()));

$string = 'Apple and {if strlen($name)>3}a lot of{else}no{/end} Bananas';

echo $STR->renderRaw($string, ['name' => 'hello']);
echo "\n";
echo $STR->renderRaw('{foreach [[1, 3], [2, 4], [3, 4]] as $test} {foreach $test as $t}{$t} {/end}{/end}');
