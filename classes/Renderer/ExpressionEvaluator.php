<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Renderer;

use JLanger\TemplateEngine\Parser\Interfaces\ExpressionEvaluatorInterface;

class ExpressionEvaluator implements ExpressionEvaluatorInterface
{
    /** {@inheritDoc} */
    public function evaluate(string $expression, array $variables)
    {
        $result = preg_replace_callback(
            '/\$([A-z0-9_]+)/',
            function ($expression) use ($variables) {
                return $this->getVariable($expression[1], $variables);
            },
            $expression
        );

        // Todo: replace eval with something less insecure
        return eval('return '.$result.';');
    }

    private function getVariable(string $variableName, $variables): string
    {
        // Todo: better debug output if variable does not exist
        $value = $variables[$variableName];

        if ($value === null) {
            return 'null';
        }
        if (is_bool($value)) {
            return $value ? 'true' : 'false';
        }
        if (is_string($value)) {
            return sprintf('"%s"', $value);
        }
        if (is_array($value)) {
            return '['.implode(',', $value).']';
        }

        return (string)$value;
    }
}
