<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Renderer;

use JLanger\TemplateEngine\Parser\Interfaces\ExpressionEvaluatorInterface;
use JLanger\TemplateEngine\Parser\Interfaces\TemplateRendererInterface;
use JLanger\TemplateEngine\Parser\Interfaces\TokenInterface;
use JLanger\TemplateEngine\Parser\Tokens\ElseifToken;
use JLanger\TemplateEngine\Parser\Tokens\ElseToken;
use JLanger\TemplateEngine\Parser\Tokens\EndToken;
use JLanger\TemplateEngine\Parser\Tokens\ExpressionToken;
use JLanger\TemplateEngine\Parser\Tokens\ForeachToken;
use JLanger\TemplateEngine\Parser\Tokens\IfToken;
use JLanger\TemplateEngine\Parser\Tokens\LiteralToken;

class TemplateRenderer implements TemplateRendererInterface
{

    /**
     * @var ExpressionEvaluatorInterface
     */
    private $expressionEvaluator;

    public function __construct(ExpressionEvaluatorInterface $expressionEvaluator)
    {
        $this->expressionEvaluator = $expressionEvaluator;
    }

    /**
     * @param TokenInterface[] $tokenArray
     * @param array            $variables
     *
     * @return string
     * @throws \Exception
     */
    public function render(array $tokenArray, array $variables): string
    {
        $string = '';
        while (count($tokenArray) > 0) {
            $token = $tokenArray[0];
            if ($token instanceof LiteralToken) {
                $string .= $token->getContent();
                array_shift($tokenArray);
                continue;
            }

            if ($token instanceof ExpressionToken) {
                $string .= $this->expressionEvaluator->evaluate($token->getExpression(), $variables);
                array_shift($tokenArray);
            } elseif ($token instanceof IfToken) {
                $string .= $this->parseCondition($tokenArray, $variables);
            } elseif ($token instanceof ForeachToken) {
                $string .= $this->parseForLoop($tokenArray, $variables);
            } else {
                throw new \Exception('Could not determine what to do with token: '. get_class($token));
            }
        }

        return $string;
    }

    private function parseForLoop(array &$tokenArray, array $variables): string
    {
        $token    = array_shift($tokenArray);
        $iterator = $this->expressionEvaluator->evaluate($token->getIteratorExpression(), $variables);
        $variable = $token->getIteratorVariable();

        // Todo: Merge with if logic
        $loopBody = [];
        $depth    = 0;
        do {
            $nextToken = array_shift($tokenArray);
            if ($depth === 0 && $nextToken instanceof EndToken) {
                break;
            }

            if ($nextToken instanceof ForeachToken) {
                $depth++;
            } elseif ($nextToken instanceof EndToken) {
                $depth--;
            }

            $loopBody[] = $nextToken;
        } while (count($tokenArray) > 0);

        $string = '';
        foreach ($iterator as $element) {
            $arrayMerge = array_merge([$variable => $element], $variables);
            $string    .= $this->render($loopBody, $arrayMerge);
        }

        return $string;
    }

    private function parseCondition(array &$tokenArray, array $variables): string
    {
        // @var IfToken $token
        $token = array_shift($tokenArray);
        $value = $this->expressionEvaluator->evaluate($token->getCondition(), $variables);

        $stopOnEndIf = static function ($nextToken) {
            return $nextToken instanceof EndToken;
        };
        $stopOnElse  = static function ($nextToken) {
            return $nextToken instanceof ElseToken || $nextToken instanceof ElseifToken;
        };

        if ($this->evaluateCondition($value)) {
            // Accumulate all tokens until else & elseif token to execute.
            $tokens = $this->partitionTokenList(
                $tokenArray,
                function ($nextToken) use ($stopOnElse, $stopOnEndIf) {
                    return $stopOnElse($nextToken) || $stopOnEndIf($nextToken);
                }
            );

            $renderedString = $this->render($tokens, $variables);

            $this->partitionTokenList($tokenArray, $stopOnEndIf);
            // Removing end token
            array_shift($tokenArray);

            return $renderedString;
        }

        // If the value is false
        // Skip all tokens that are not else or elseif or /if
        do {
            $this->partitionTokenList(
                $tokenArray,
                static function ($token) use ($stopOnEndIf, $stopOnElse) {
                    return $stopOnElse($token) || $stopOnEndIf($token);
                }
            );
            $elseToken = array_shift($tokenArray);

            if ($elseToken instanceof ElseifToken) {
                $value = $this->evaluateCondition(
                    $this->expressionEvaluator->evaluate($elseToken->getCondition(), $variables)
                );
            } else {
                $value = true;
            }
        } while (!$value);

        $block = $this->partitionTokenList(
            $tokenArray,
            static function ($token) use ($stopOnEndIf, $stopOnElse) {
                return $stopOnElse($token) || $stopOnEndIf($token);
            }
        );

        $renderedString = $this->render($block, $variables);

        $this->partitionTokenList($tokenArray, $stopOnEndIf);

        // Removing the end token
        array_shift($tokenArray);

        return $renderedString;
    }

    private function evaluateCondition($value): bool
    {
        // Todo: add debug output to where the error occurs
        if (!is_bool($value)) {
            trigger_error('If condition must be a boolean', E_USER_WARNING);
        }

        return (bool)$value;
    }

    private function partitionTokenList(array &$tokenArray, callable $isStopToken): array
    {
        $tokens = [];
        $depth  = 0;
        do {
            $nextToken = array_shift($tokenArray);
            if ($depth === 0 && $isStopToken($nextToken)) {
                array_unshift($tokenArray, $nextToken);
                break;
            }

            if ($nextToken instanceof IfToken) {
                $depth++;
            } elseif ($nextToken instanceof EndToken) {
                $depth--;
            }

            $tokens[] = $nextToken;
        } while (count($tokenArray) > 0);

        return $tokens;
    }
}
