<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine;

use Exception;
use JLanger\TemplateEngine\Parser\Interfaces\TemplateParserInterface;
use JLanger\TemplateEngine\Parser\Interfaces\TemplateRendererInterface;

class TemplateEngine implements TemplateEngineInterface
{

    /**
     * @var string
     */
    private $baseDir;

    /**
     * @var TemplateParserInterface
     */
    private $templateParser;

    /**
     * @var TemplateRendererInterface
     */
    private $templateRenderer;

    /**
     * TemplateRenderer constructor.
     *
     * @param TemplateParserInterface   $templateParser
     * @param TemplateRendererInterface $templateRenderer
     * @param string                    $baseDir
     */
    public function __construct(
        TemplateParserInterface $templateParser,
        TemplateRendererInterface $templateRenderer,
        ?string $baseDir = null
    ) {
        $this->baseDir          = rtrim($baseDir ?? __DIR__, DIRECTORY_SEPARATOR);
        $this->templateParser   = $templateParser;
        $this->templateRenderer = $templateRenderer;
    }

    /**
     * Reads the template file and returns its content
     *
     * @param string $templatePath
     *
     * @return string
     * @throws Exception
     */
    private function readTemplateFile(string $templatePath): string
    {
        $filePath = $templatePath;
        if ($templatePath[0] !== DIRECTORY_SEPARATOR) {
            $filePath = join(DIRECTORY_SEPARATOR, [$this->baseDir, $templatePath]);
        }

        if (!file_exists($filePath) || is_dir($filePath)) {
            throw new Exception("File $filePath does not exist");
        }

        $content = file_get_contents($filePath);

        if ($content === false) {
            throw new Exception("Could not read template content of file: $templatePath");
        }

        return $content;
    }

    public function render(string $templatePath, array $variables = []): string
    {
        $content = $this->readTemplateFile($templatePath);

        return $this->renderRaw($content, $variables);
    }

    public function renderRaw(string $content, array $variables = []): string
    {
        $tokens = $this->templateParser->parse($content);

        return $this->templateRenderer->render($tokens, $variables);
    }
}
