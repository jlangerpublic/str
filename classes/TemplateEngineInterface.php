<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine;

interface TemplateEngineInterface
{
    /**
     * Renders a template (with a given file name) and some variables
     *
     * @param string $templatePath
     * @param array  $variables
     *
     * @return string
     */
    public function render(string $templatePath, array $variables = []): string;

    /**
     * Renders a raw template string with given variables
     *
     * @param string $content
     * @param array  $variables
     *
     * @return string
     */
    public function renderRaw(string $content, array $variables = []): string;
}
