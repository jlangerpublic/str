<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Parser;

use Exception;
use JLanger\TemplateEngine\Parser\Interfaces\CommandTokenInterface;
use JLanger\TemplateEngine\Parser\Tokens\ElseifToken;
use JLanger\TemplateEngine\Parser\Tokens\ElseToken;
use JLanger\TemplateEngine\Parser\Tokens\EndToken;
use JLanger\TemplateEngine\Parser\Tokens\ForeachToken;
use JLanger\TemplateEngine\Parser\Tokens\IfToken;

class CommandTokenParser
{

    /**
     * Maps the individual contents to a command token
     *
     * @param string $content
     *
     * @return CommandTokenInterface
     * @throws Exception
     */
    public function parse(string $content): CommandTokenInterface
    {
        $tokenClasses = [
            'if' => IfToken::class,
            'else' => ElseToken::class,
            'elseif' => ElseifToken::class,
            'foreach' => ForeachToken::class,
            '/end' => EndToken::class
        ];

        foreach ($tokenClasses as $keyword => $tokenClass) {
            if (stripos($content, $keyword) === 0) {
                return new $tokenClass($content);
            }
        }

        throw new Exception('Unknown command');
    }
}
