<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Parser\Interfaces;

interface TemplateRendererInterface
{
    /**
     * Renders a list of tokens into a string
     *
     * @param array $tokenArray
     * @param array $variables
     *
     * @return string
     */
    public function render(array $tokenArray, array $variables): string;
}
