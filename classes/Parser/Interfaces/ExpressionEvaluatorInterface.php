<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Parser\Interfaces;

interface ExpressionEvaluatorInterface
{
    /**
     * @param string $expression
     * @param array  $variables
     *
     * @return mixed
     */
    public function evaluate(string $expression, array $variables);
}
