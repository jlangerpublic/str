<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Parser\Interfaces;

interface TemplateParserInterface
{
    /**
     * Parses an HTML documents into tokens
     *
     * @param string $content
     *
     * @return TokenInterface[]
     */
    public function parse(string $content): array;
}
