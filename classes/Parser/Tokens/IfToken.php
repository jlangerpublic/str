<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Parser\Tokens;

use JLanger\TemplateEngine\Parser\Interfaces\CommandTokenInterface;

class IfToken implements CommandTokenInterface
{
    /**
     * @var string
     */
    private $content;

    public function __construct(string $content)
    {
        $this->content = $content;
    }

    public function getCondition(): string
    {
        return substr($this->content, 3);
    }
}
