<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Parser\Tokens;

use JLanger\TemplateEngine\Parser\Interfaces\TokenInterface;

class ExpressionToken implements TokenInterface
{
    private $expression;

    public function __construct(string $expression)
    {
        $this->expression = $expression;
    }

    public function getExpression(): string
    {
        return $this->expression;
    }
}
