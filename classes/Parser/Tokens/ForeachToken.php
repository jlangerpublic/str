<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Parser\Tokens;

use JLanger\TemplateEngine\Parser\Interfaces\CommandTokenInterface;

class ForeachToken implements CommandTokenInterface
{
    /**
     * @var string
     */
    private $iteratorExpression;

    /**
     * @var string
     */
    private $iteratorVariable;

    public function __construct(string $content)
    {
        $matches = [];
        if (preg_match('/^foreach (.*) as \$([A-z0-9_]+)$/', $content, $matches) !== false) {
            $this->iteratorExpression = $matches[1];
            $this->iteratorVariable   = $matches[2];
            return;
        }

        throw new \Exception('Could not parse foreach Token.');
    }

    public function getIteratorExpression(): string
    {
        return $this->iteratorExpression;
    }

    public function getIteratorVariable(): string
    {
        return $this->iteratorVariable;
    }
}
