<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Parser\Tokens;

use JLanger\TemplateEngine\Parser\Interfaces\TokenInterface;

class LiteralToken implements TokenInterface
{
    /**
     * @var string
     */
    private $content;

    public function __construct(string $literal = '')
    {
        $this->content = $literal;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
