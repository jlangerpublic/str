<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Parser\Tokens;

use JLanger\TemplateEngine\Parser\Interfaces\CommandTokenInterface;

class EndToken implements CommandTokenInterface
{
    public function __construct(string $content)
    {
    }
}
