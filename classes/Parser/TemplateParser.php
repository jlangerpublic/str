<?php
declare(strict_types=1);

namespace JLanger\TemplateEngine\Parser;

require __DIR__ . '/../../vendor/autoload.php';

use JLanger\TemplateEngine\Parser\Interfaces\TemplateParserInterface;
use JLanger\TemplateEngine\Parser\Interfaces\TokenInterface;
use JLanger\TemplateEngine\Parser\Tokens\ExpressionToken;
use JLanger\TemplateEngine\Parser\Tokens\LiteralToken;

class TemplateParser implements TemplateParserInterface
{
    /**
     * @var CommandTokenParser
     */
    private $commandTokenParser;

    public function __construct(
        CommandTokenParser $commandTokenParser
    ) {
        $this->commandTokenParser = $commandTokenParser;
    }

    /**
     * Parses an HTML documents into tokens
     *
     * @param string $content
     *
     * @return TokenInterface[]
     */
    public function parse(string $content): array
    {
        $tokenList          = [];
        $nextTokenIsCommand = false;
        while ($content !== '') {
            // Set the new command state to the command state from the previous loop
            $isCommand = $nextTokenIsCommand;
            // If the current state is that we are not parsing a command, then wait for an open brace
            if (!$isCommand) {
                $nextDelimiter      = strpos($content, '{');
                $nextTokenIsCommand = true;
            } else {
                // If we have a command then wait for a closing brace.
                $nextDelimiter      = strpos($content, '}');
                $nextTokenIsCommand = false;
            }
            // If no brace was found there are no new tokens til the end of the document
            // so we just put the rest of the text into a single token
            if ($nextDelimiter === false) {
                $tokenList[] = $this->parseToken($content, $isCommand);
                break;
            }

            // Finding the text that is the token's content
            $text = substr($content, 0, $nextDelimiter);
            // Making a new token and adding it to the list
            $tokenList[] = $this->parseToken($text, $isCommand);

            // The rest after the token is the new content
            $content = substr($content, $nextDelimiter + 1);
        }

        return $tokenList;
    }

    private function parseToken(string $content, bool $isCommand): TokenInterface
    {
        // If it's not a command then it is a literal
        if (!$isCommand) {
            return new LiteralToken($content);
        }

        // If the expression starts with a $ sign it's a variable
        if (strpos($content, '$') === 0) {
            return new ExpressionToken($content);
        }

        // Otherwise it's a command (if it's not this will throw an error)
        return $this->commandTokenParser->parse($content);
    }
}

/*
 *    $template = new TemplateParser(new CommandTokenParser());
 *    $token = $template->parse('Hallo ich bin ein Text. Variable: {$var} und ein if {if $bla}bla{/if} {foreach
 * $a}{/foreach}');
 *    var_dump($token);
 */
